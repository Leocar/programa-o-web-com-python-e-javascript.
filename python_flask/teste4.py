from flask import Flask, render_template # Falando de render em algo que está na pasta template

app = Flask(__name__)

@app.route("/")

def index():
    names = ["Alice","Leonardo","David"]
    return render_template("index.html", names=names)#Dando render