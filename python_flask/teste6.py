from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("indexu.html")

@app.route("/hi" ,methods=["POST"])
def teste():
    nome = request.form.get("name") 
    return render_template("ola.html", nome=nome)