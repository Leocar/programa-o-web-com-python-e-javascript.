#Importando as bibliotecas

import os

from flask import Flask, render_template, request
from sqlalchemy import create_engine
from sqlalchemy.orm import scoperd_session, sessionmaker

app = Flask(__name__)

#Chamada do BD
engine = create_engine(os.getenv("DATABASE_URL"))
db = scoperd_session(sessionmaker(bind=engine))

@app.route("/")
def index():
    flights = db.execute("SELECT * FROM flights").fetchall()
    return render_template("voos.html", flights=flights)

@app.route("/livros", methods=["POST"])
def livro():
    #Pegando a Informação
    name = request.from.get("name")
    try:
        flight_id = int(request.form.get("fligth_id"))
    except ValueError:
        return render_template("erro.html"), message="Numero de vôo invalido"

    #Verificação de dados

    id db.execute("SELECT * FROM flights WHERE id = :id",{"id": flight_id}).rowcount == 0:
        return render_template("erro.html"), message="Não foi possível encontrar vôos com esse id"
    db.execute("INSERT INTO passagers (name, flight_id) VALUES (:name, :flight_id)",{"name": name, "flight_id": flight_id})
    db.commit()
    return render_template("sucesso.html")