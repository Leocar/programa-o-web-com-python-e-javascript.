# Import das bibliotecas
import csv
import os 

from sqlalchemy import create_engine
from sqlalchemy.orm import scoperd_session, sessionmaker

#Chamada do BD
engine = create_engine(os.getenv("DATABASE_URL"))
db = scoperd_session(sessionmaker(bind=engine))

# Função de chamada da query para selecionar todos os voos
def main():
   
   reader = csv.reader(f) #çendo um arquivo csv para adicionar as informações dele no banco de dados
   for origin, destination, duration in reader:
        db.execute("INSERT INTO flights(origin, destination, duration) VALUES(:origin,:destination,:duration)",{"origin": origin, "destination": destination, "duration": duration}) #Executando a querry com um dicionario de dados para passar os aquivos do csv para o banco
        print(f"Adicionando voos de {origin} para {destination} ultimo {duration} minutos") 
    db.commit() #Bd commit é como se você estivesse dando um save no banco 
if __name__ == "__main__":
    main()