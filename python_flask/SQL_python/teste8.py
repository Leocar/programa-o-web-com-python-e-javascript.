
# Import das bibliotecas

import os 

from sqlalchemy import create_engine
from sqlalchemy.orm import scoperd_session, sessionmaker

#Chamada do BD
engine = create_engine(os.getenv("DATABASE_URL"))
db = scoperd_session(sessionmaker(bind=engine))

# Função de chamada da query para selecionar todos os voos
def main():
    flights = db.execute("SELECT origin, destination,duration FROM flights")
    for flight in flights: 
        print(f"{flight.origin}para{flight.destination},{flight.duration}") #Listando Todos o voos na tela
if __name__ == "__main__":
    main()