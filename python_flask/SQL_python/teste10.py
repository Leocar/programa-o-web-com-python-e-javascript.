# Import das bibliotecas
import os 

from sqlalchemy import create_engine
from sqlalchemy.orm import scoperd_session, sessionmaker

#Chamada do BD
engine = create_engine(os.getenv("DATABASE_URL"))
db = scoperd_session(sessionmaker(bind=engine))

def main():
    
    flights = db.execute("SELECT id,origin,destination,duration FROM flights").
    for flight in flights:
        print(f"O vôo {flight.id}: {flight.origin} para {flight.destination}, {flight.duration} minutos.")

# Pesquisa de vôos por ID

flight_id = int(input("\n Insira o ID do vôo"))
flight =  db.execute("SELECT origin, destination, duration FORM flights WHERE id = :id", {"id": flight_id}).fetchone()

# Verificando se o vôo informado é válido

if flight is None:
    print("Erro: Vôo não encontrado")
    return

#Lista de Passageiros
passageiros = db.execute("SELECT name FROM passangers WHERE flight_id = :flight_id",{"flight_id": flight_id}).fetchall()
print("\n Passageiros:")
for passageiro in passageiros
    print(passageiro.name)
if len(passageiros) ==0:
    print("Este vôo não tem passagueiros")
    

