from flask import Flask, render_template # Falando de render em algo que está na pasta template

app = Flask(__name__)

@app.route("/")

def index():
    headline = "Olá Mundo!"
    return render_template("index.html", headline=headline)#Dando render

@app.route("/bye")

def bye():
    headline ="Tchau!!"
    return render_template("index.html", headline=headline)#Dando render

