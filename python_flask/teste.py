from flask import Flask, render_template # Falando de render em algo que está na pasta template

app = Flask(__name__)

@app.route("/")

def index():
    return render_template("index.html")#Dando render

@app.route("/leonardo")
def leo():
    return "Olá leonardo!"

@app.route("/<string:name>") #pega o name digitado na url
def hello(name): #mada para def
   name = name.capitalize() # Capturando o name 
   a = 'teste'
   return f"Ola, {name}!\n" + a #processa no return



#export FLASK_APP=teste.py
#export FLASK_ENV=development
#flask run
